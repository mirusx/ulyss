# README #


### Qucik summary ###

This is a proof-of-concept prototype for SCU management based on elasticity principles.
 
It includes:

*  a model for ICUs, Tasks, SCUs, as well as a metric model for ICUs and SCUs;
*  algorithms for ICU ranking and SCU adaptation;
*  a process engine to run a proof-of-concept process for adapting SCUs based on SLA changes.


### Running examples ###

* Build the project using Apache Maven
* Run the demo classes in Ulyss/ulys/icu-manager/src/main/java/dsg/infosys/demo / 


### Who do I talk to? ###

Mirela Riveni and Tien-Dung Nguyen